package com.xiaoshuai.plat.controller.pc.service;

import java.util.List;

import com.xiaoshuai.plat.pojo.Menu;
/**
 * 相关接口类
 * @author 小帅帅丶
 * @Title CommonService
 * @时间   2017-2-7下午4:31:54
 */
public interface HomePageService {
	  public List<Menu> getAllMenu();
}
