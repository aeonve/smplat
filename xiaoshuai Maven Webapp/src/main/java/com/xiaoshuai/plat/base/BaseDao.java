package com.xiaoshuai.plat.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.xiaoshuai.plat.pojo.Menu;


/**
 * SpringMVC+Hibernate +MySql+ EasyUI ---CRUD
 * dao层的共有父类，提供了常用的数据库操作方法。</br>
 * 
 *
 */
@Repository("baseDao")
public class BaseDao{
	
	private static Log log = LogFactory.getLog(BaseDao.class);
	@Autowired
	protected JdbcTemplate jdbcTemplate;
//	@Autowired
//	protected SessionFactory sessionFactory;
	
	 /**
    * 把普通sql语句,处理成分页语句</br>
    * @param sql		纯sql语句
    * @param start		起始页码
    * @param size		每页个数
    * @return
    */
   public String getLimitString(String sql, int start, int size){
   	//只有在页数不为负且每页为正的时候,才进行分页
   	if(start >= 0 && size > 0){
   		sql = sql + " limit " + start*size +", " + size;
   	}
   	return sql;
   }
   /**
    * 纯sql的分页
    * @param sql		纯sql语句
    * @param param		参数
    * @param start		分页起始数
    * @param size		每页个数
    * @param order		排序语句,本方法会自动加上" order by "关键字
    * @return			以Map<String,Object>组成的列表,可能返回null
    */
   public List<Map<String,Object>> listByNative(String sql, Object[] param, int start, int size, String order){
   	if(null != order && order.trim().length() > 0){
   		sql += " order by " + order;
   	}
   	sql = getLimitString(sql, start, size);
   	return jdbcTemplate.queryForList(sql, param);
   }
   /**
    * 纯sql的count方法
    * @param sql		纯sql语句
    * @param param		参数
    * @return			正常的个数,如果出错或为空,返回0
    */
   public int countByNative (String sql, Object[] param){
   	return jdbcTemplate.queryForObject(sql, param, Integer.class);
   }
   /**
    * 纯sql的分页
    * @param sql		纯sql语句
    * @param param		参数
    * @param start		分页起始数
    * @param size		每页个数
    * @param order		排序语句,本方法会自动加上" order by "关键字
    * @return			以Map<String,Object>组成的列表,可能返回null
    */
   public List<Map<String,Object>> findByAll(String sql, Object[] param, int start, int size, String order){
   	if(null != order && order.trim().length() > 0){
   		sql += " order by " + order;
   	}
   	sql = getLimitString(sql, start, size);
   	return jdbcTemplate.queryForList(sql, param);
   }
   /**
    * 查询所有的数据
    * @param sql
    * @return
    * @remark 后续可以增加到某用户所涉及的菜单进行加载
    */
	public List<Map<String, Object>> findByAll(String sql) {
		return jdbcTemplate.queryForList(sql);
	}
}
