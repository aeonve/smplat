package com.xiaoshuai.plat.vo.message;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xiaoshuai.plat.util.weixin.WeixinUtil;
import com.xiaoshuai.plat.vo.AccessToken;
import com.xiaoshuai.plat.vo.TemplateData;
import com.xiaoshuai.plat.vo.WxTemplate;

public class SendOrderPaySuccessMsg {
	
	Logger log = LoggerFactory.getLogger(getClass());

	/**
     * 发送模板消息
     * appId 公众账号的唯一标识
     * appSecret 公众账号的密钥
     * openId 用户标识
     */
	public void send_template_message(String appId, String appSecret, String openId) {
		AccessToken token = WeixinUtil.getAccessToken(appId, appSecret);
		String access_token = token.getToken();
//		String access_token = "YZfU2MNm9LzPNDRVeGf01BCQk4PLs-MGII-i8zVa9gWHyTgzGmmTmgSrlp002wF_lsBHrvpXbXa_P9AhrYR1EdOXz0KHe11dNFD4OknIIZ0uBU6PZvMDY5FYjrcd4Ys7AAVhACAEEZ";
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+access_token;
		WxTemplate temp = new WxTemplate();
		temp.setUrl("http://mp.weixin.qq.com/s?__biz=MzI0NDU0MTE2MQ==&mid=100000019&idx=1&sn=e8d6c725c3537fc44f11a5d79a10ba3e#rd");
		temp.setTouser(openId);
		temp.setTopcolor("#000000");
		temp.setTemplate_id("fEPof7ryl17f0gtKceS0swO5JQMwHvor4viE2P6GoMs");
		Map<String,TemplateData> m = new HashMap<String,TemplateData>();
		TemplateData first = new TemplateData();
	    first.setColor("#000000");  
	    first.setValue("***标题***嗖嗖嗖，您的香皂已发货，我们正加速送到您的手上");  
	    m.put("first", first);  
	    TemplateData name = new TemplateData();  
	    name.setValue("***订单内容***香皂");  
	    m.put("name", name);
	    TemplateData wuliu = new TemplateData();  
	    wuliu.setColor("#000000");  
	    wuliu.setValue("***物流服务***顺丰即日达");  
	    m.put("wuliu", wuliu);
	    TemplateData orderNo = new TemplateData();  
	    orderNo.setColor("#000000");  
	    orderNo.setValue("***快递单号***XW5244030005646");  
	    m.put("orderNo", orderNo);
	    TemplateData receiveAddr = new TemplateData();  
	    receiveAddr.setColor("#000000");  
	    receiveAddr.setValue("***收货信息***矩阵国际");  
	    m.put("receiveAddr", receiveAddr);
	    TemplateData remark = new TemplateData();  
	    remark.setColor("#000000");  
	    remark.setValue("***备注说明***");  
	    m.put("Remark", remark);ttp://www.cooco.net.cn/
	    temp.setData(m);
	    String jsonString = JSONObject.fromObject(temp).toString();
	    JSONObject jsonObject = WeixinUtil.httpRequest(url, "POST", jsonString);
        System.out.println(jsonObject);
        int result = 0;
        if (null != jsonObject) {  
             if (0 != jsonObject.getInt("errcode")) {  
                 result = jsonObject.getInt("errcode");
                 log.error("错误 errcode:{} errmsg:{}", jsonObject.getInt("errcode"), jsonObject.getString("errmsg"));  
             }  
         }
        log.info("模板消息发送结果："+result);
	}
	public static void main(String[] args) {
		SendOrderPaySuccessMsg msg = new SendOrderPaySuccessMsg();
		String openid = "o2VKNju8JqCeGVoEWJ1S8Ue_up89,o2VKNju8JqCeGVoEWJ1S8Ue_up89";
		String openids[] = openid.split(",");
//		msg.send_template_message("wx2d39c6c31ed5f144", "a593fa37e25ddc96027462ec2c3fa2ce", "o2VKNju8JqCeGVoEWJ1S8Ue_up8E");
		for (int i = 0; i < openids.length; i++) {
			msg.send_template_message("wx2d39c6c31ed5f144", "a593fa37e25ddc96027462ec2c3fa2ce", openids[i].trim());
			System.out.println(openids[i]);
		}
	}
//	public static void main(String[] args) {
//		WeixinUtil util = new WeixinUtil();
//		Batchget batchget = new Batchget();
//		batchget.setType("news");
//		batchget.setOffset(0);
//		batchget.setCount(20);
//		JSONObject jsonObject = JSONObject.fromObject(batchget);
//		util.batchgetMaterial("wxe4925d3249baf6cf", "c246fa4d6c3af9bb154ced5a1b302705", jsonObject.toString());
//	}
}
