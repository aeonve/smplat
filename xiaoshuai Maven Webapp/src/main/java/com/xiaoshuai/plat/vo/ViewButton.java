package com.xiaoshuai.plat.vo;
/**
 * 
 * @author 宗潇帅
 * @修改日期 2014-7-15上午9:58:52
 */
public class ViewButton extends Button{
	private String type;
	private String url;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	

}
