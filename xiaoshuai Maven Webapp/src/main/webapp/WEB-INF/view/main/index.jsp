<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%  
        String path = request.getContextPath();  
        String basePath = request.getScheme() + "://"  
                + request.getServerName() + ":" + request.getServerPort()  
                + path ;  
    %>
<html>
<head>
<jsp:include page="/common/meta.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>手风琴菜单</title>
<script type="text/javascript">  
$(function(){
	$('#tabs').tabs('add',{
		title:'主页',
		tools:[{
	        iconCls:'icon-mini-refresh',
	        handler:function(){
	        	var tab = $('#tabs').tabs('getSelected');
	        	tab.panel('refresh', '<%=basePath%>/main.jsp');
	        }
	    }],
        href:'<%=basePath%>/main.jsp',
        closable:false
	});
	$('.easyui-tree').tree({
		onClick : function(node) {
		if (node.attributes && node.attributes.url) {
			var url = '${ctx}' + node.attributes.url;
			addTab({
				url : node.attributes.url,
				title : node.text,
				iconCls : node.icon
				});
			}
		}
    });
	function addTab(params) {
		var iframe = '<iframe src="' + params.url + '" frameborder="0" style="border:0;width:100%;height:99.5%;"></iframe>';
		var t = $('#tabs');
		var opts = {
			title : params.title,
			closable : true,
			iconCls : params.iconCls,
			content : iframe,
			border : false,
			fit : true
		};
		if (t.tabs('exists', opts.title)) {
			t.tabs('select', opts.title);
		} else {
			t.tabs('add', opts);
		}
	}

});
    </script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',noheader:true,split:false"
		style="height:66px;">
		<div id="header">
			<span style="float: right; padding-right: 20px;">欢迎 <b>${sessionInfo.name}</b>&nbsp;&nbsp; <a href="javascript:void(0)" onclick="editUserPwd()" style="color: #fff">修改密码</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="logout()" style="color: #fff">安全退出</a>
        	&nbsp;&nbsp;&nbsp;&nbsp;
    		</span>
    		<span class="header"></span>
    	</div>
	</div>
	<div data-options="region:'west',title:'菜单',split:true" style="width:200px;">
		<div class="easyui-accordion" data-options="border:false" id='menu'>
			${menus}
		</div>
	</div>
	<div data-options="region:'center'," style="padding:1px;">
		<div id='tabs' class="easyui-tabs"
			data-options="fit:true,border:false"></div>
	</div>
	<div data-options="region:'south',noheader:true,split:false"
		style="height:20px;"></div>
</body>
</html>
